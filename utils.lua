--[[
The MIT License (MIT)
Copyright (c) 2019 Graham Ranson - www.grahamranson.co.uk / @GrahamRanson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]

--- Class creation.
local Utils = {}
Utils.__index = Utils

--- Required libraries.
local json = require( "json" )

--- Localised functions.
local decode = json.decode
local pathForFile = system.pathForFile
local open = io.open
local close = io.close

--- Initiates a new Utils object.
-- @return The new object.
function Utils:new()

	local instance = setmetatable( {}, self )

	return instance

end

--- Decodes a Json string into a table.
-- @param string The string to decode.
-- @return The decoded table.
function Utils:jsonDecode( string )
	return decode( string or "" )
end

--- Decodes the contents of a file that has been encoded as json.
-- @param path The path to the file.
-- @param baseDir The directory that the file resides in. Optional, defaults to system.DocumentsDirectory.
-- @return The decoded file as a table.
function Utils:decodeFile( path, baseDir )
	return self:jsonDecode( self:readInFile( path, baseDir ) )
end

--- Reads in a file from disk.
-- @param path The path to the file.
-- @param baseDir The directory that the file resides in. Optional, defaults to system.ResourceDirectory.
-- @return The contents of the file, or an empty string if the read failed.
function Utils:readInFile( path, baseDir )

	local path = pathForFile( path, baseDir or system.ResourceDirectory )

	if path then

		local file = open( path, "r" )

		if file then

			local contents = file:read( "*a" ) or ""

			close( file )

			file = nil

			return contents

		end

	end

end

--- Destroys this Utils object.
function Utils:destroy()

end

--- Return the Utils class definition.
return Utils
