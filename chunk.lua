--[[
The MIT License (MIT)
Copyright (c) 2019 Graham Ranson - www.grahamranson.co.uk / @GrahamRanson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]

--- Class creation.
local Chunk = {}
Chunk.__index = Chunk

--- Required libraries.
local Tile = require( "tella.tile" )

--- Localised functions.
local ceil = math.ceil
local floor = math.floor
local remove = table.remove

--- Initiates a new Chunk object.
-- @param params Paramater table for the object.
-- @return The new object.
function Chunk:new( map, layer, params )

	local instance = setmetatable( {}, self )

    instance._map = map
	instance._layer = layer

    instance._data = params.data

	instance._width = params.width
    instance._height = params.height

    instance._x = params.x
    instance._y = params.y

	local tileSize = instance._map:getTileSize( true )

    instance._tiles = {}

    instance._snapshot = display.newSnapshot( ( instance:getWidth() * tileSize.width ) * 2, ( instance:getHeight() * tileSize.height * 2 ) )

    instance._map:getVisual():insert( instance._snapshot )

	local data = instance:getData()
    for i = 1, #data, 1 do
        instance._tiles[ i ] = Tile:new( instance._map, { width = tileSize.width, height = tileSize.height, x = floor( ( i - 1 ) % instance:getWidth() ), y = floor( ( i - 1 ) / instance:getHeight() ), id = data[ i ] } )
        if instance._tiles[ i ]:getVisual() then
            instance:getSnapshot().group:insert( instance._tiles[ i ]:getVisual() )
        end
    end

    instance:getSnapshot().x = ( instance:getX() * tileSize.width ) - floor( ( 1 * instance:getX() / tileSize.width ) ) + instance._layer:getOffsetX()
    instance:getSnapshot().y = ( instance:getY() * tileSize.height ) - floor( ( 1 * instance:getY() / tileSize.height ) ) + instance._layer:getOffsetY()

	return instance

end

--- Shows this Chunk object.
function Chunk:show()

    for i = 1, #self._tiles, 1 do
        self._tiles[ i ]:show()
        if self._tiles[ i ]:getVisual() then
            self:getSnapshot().group:insert( self._tiles[ i ]:getVisual() )
        end
    end

    self:draw()

end

--- Hides this Chunk object.
function Chunk:hide()

    for i = 1, #self._tiles, 1 do
        self._tiles[ i ]:hide()
    end

    self:draw()

end

--- Invalidates the underlying Snapshot for this Chunk object.
function Chunk:draw()
    if self:getSnapshot() and self:getSnapshot()[ "invalidate" ] then
        self:getSnapshot():invalidate()
    end
end

--- Gets the tile data of this Chunk.
-- @return The data.
function Chunk:getData()
	return self._data
end

--- Gets the tiles of this Chunk.
-- @return The tiles.
function Chunk:getTiles()
	return self._tiles
end

--- Gets the position of this Chunk.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'x' and 'y' properties or as two separate return values.
function Chunk:getPosition( asTable )
    if asTable then
        return { x = self:getX(), y = self:getY() }
    else
        return self:getX(), self:getY()
    end
end

--- Gets the x position of this Chunk.
-- @return The position.
function Chunk:getX()
	return self._x
end

--- Gets the y position of this Chunk.
-- @return The position.
function Chunk:getY()
	return self._y
end

--- Gets the content position of this Chunk.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'x' and 'y' properties or as two separate return values.
function Chunk:getContentPosition( asTable )
    if asTable then
        return { x = self:getContentX(), y = self:getContentY() }
    else
        return self:getContentX(), self:getContentY()
    end
end

--- Gets the x content position of this Chunk.
-- @return The position.
function Chunk:getContentX()
	return self:getSnapshot().x
end

--- Gets the y content position of this Chunk.
-- @return The position.
function Chunk:getContentY()
	return self:getSnapshot().y
end

--- Gets the size of this Chuink.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'width' and 'height' properties or as two separate return values.
function Chunk:getSize( asTable )
    if asTable then
        return { width = self:getWidth(), height = self:getHeight() }
    else
        return self:getWidth(), self:getHeight()
    end
end

--- Gets the width of this Chunk.
-- @return The width.
function Chunk:getWidth()
	return self._width
end

--- Gets the eight of this Chunk.
-- @return The height.
function Chunk:getHeight()
	return self._height
end

--- Gets the content size of this Chuink.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'width' and 'height' properties or as two separate return values.
function Chunk:getContentSize( asTable )
    if asTable then
        return { width = self:getContentWidth(), height = self:getContentHeight() }
    else
        return self:getContentWidth(), self:getContentHeight()
    end
end

--- Gets the content width of this Chunk.
-- @return The width.
function Chunk:getContentWidth()
	return self:getSnapshot().contentWidth * 0.5
end

--- Gets the hcontent eight of this Chunk.
-- @return The height.
function Chunk:getContentHeight()
	return self:getSnapshot().contentHeight * 0.5
end

--- Gets the snapshot of this Chunk.
-- @return The snapshot.
function Chunk:getSnapshot()
	return self._snapshot
end

--- Sets the opacity of this chunk object.
-- @param opacity The opacity value.
function Chunk:setOpacity( opacity )
	self:getSnapshot().alpha = opacity
end

--- Destroys this Chunk object.
function Chunk:destroy()

    for i = #self._tiles, 1, -1 do
        local t = remove( self._tiles, i )
        t:destroy()
        t = nil
    end

	display.remove( self._snapshot )
	self._snapshot = nil

end

--- Return the Chunk class definition.
return Chunk
