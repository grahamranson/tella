--[[
The MIT License (MIT)
Copyright (c) 2019 Graham Ranson - www.grahamranson.co.uk / @GrahamRanson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]

--- Class creation.
local Map = {}
Map.__index = Map

--- Required libraries.
local Layer = require( "tella.layer" )
local Tileset = require( "tella.tileset" )
local Utils = require( "tella.utils" ):new()

--- Localised functions.
local remove = table.remove

--- Initiates a new Map object.
-- @param params Paramater table for the object.
-- @return The new object.
function Map:new( params )

	local instance = setmetatable( {}, self )

    instance._path = params.path

    instance._data = Utils:decodeFile( instance._path, system.ResourceDirectory )

    instance._layers = {}
    instance._tilesets = {}

    instance._visual = display.newGroup()

    for i = 1, #instance._data.tilesets, 1 do
        instance._tilesets[ i ] = Tileset:new( instance, instance._data.tilesets[ i ] )
    end

    for i = 1, #instance._data.layers, 1 do
        instance._layers[ i ] = Layer:new( instance, instance._data.layers[ i ] )
    end

	return instance

end

--- Gets the visual display object of this map.
-- @return The display object.
function Map:getVisual()
    return self._visual
end

--- Gets the type of this map.
-- @return The type.
function Map:getType()
	return self._data.type
end

--- Gets the version number of this map.
-- @return The version number.
function Map:getVersion()
	return self._data.version
end

--- Gets the render order of this map.
-- @return The render order.
function Map:getRenderOrder()
	return self._data.renderorder
end

--- Gets the orientation of this map.
-- @return The orientation.
function Map:getOrientation()
	return self._data.orientation
end

--- Gets the version of Tiled this map was created by.
-- @return The version number.
function Map:getTiledVersion()
	return self._data.tiledversion
end

--- Gets the next layer ID of this map.
-- @return The layer ID.
function Map:getNextLayerID()
	return self._data.nextlayerid
end

--- Gets the next object ID of this map.
-- @return The object ID.
function Map:getNextObjectID()
	return self._data.nextobjectid
end

--- Checks if this map infinite or not.
-- @return True if it is, false otherwise.
function Map:isInfinite()
    return self._data.infinite
end

--- Gets the size of the map.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'width' and 'height' properties or as two separate return values.
function Map:getSize( asTable )
    if asTable then
		return { width = self:getWidth(), height = self:getHeight() }
	else
		return self:getWidth(), self:getHeight()
	end
end

--- Gets the width of the map.
-- @return The width.
function Map:getWidth()
	return self._data.width
end

--- Gets the height of the map.
-- @return The height.
function Map:getHeight()
	return self._data.height
end

--- Gets the size of the tiles used in this map.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'width' and 'height' properties or as two separate return values.
function Map:getTileSize( asTable )
    if asTable then
		return { width = self:getTileWidth(), height = self:getTileHeight() }
	else
		return self:getTileWidth(), self:getTileHeight()
	end
end

--- Gets the width of the tiles in this map.
-- @return The width.
function Map:getTileWidth()
	return self._data.tilewidth
end

--- Gets the height of the tiles in this map.
-- @return The height.
function Map:getTileHeight()
	return self._data.tileheight
end

function Map:getTilesetFromGID( gid )

    for i = 1, #self._tilesets, 1 do
        if gid >= self._tilesets[ i ]:getFirstGID() and gid <= self._tilesets[ i ]:getTileCount() then
            return self._tilesets[ i ]
        end
    end

end

function Map:show( position, bounds )
    for i = 1, #self._layers, 1 do
        self._layers[ i ]:show( position, bounds )
    end
end

--- Destroys this Map object.
function Map:destroy()

	for i = #self._layers, 1, -1 do
        local l = remove( self._layers, i )
        l:destroy()
        l = nil
    end

	for i = #self._tilesets, 1, -1 do
        local t = remove( self._tilesets, i )
        t:destroy()
        t = nil
    end

	display.remove( self._visual )
	self._visual = nil

end

--- Return the Map class definition.
return Map
