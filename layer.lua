--[[
The MIT License (MIT)
Copyright (c) 2019 Graham Ranson - www.grahamranson.co.uk / @GrahamRanson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]

--- Class creation.
local Layer = {}
Layer.__index = Layer

--- Required libraries.
local Chunk = require( "tella.chunk" )
local Property = require( "tella.property" )

--- Localised functions.
local remove = table.remove

--- Initiates a new Layer object.
-- @param params Paramater table for the object.
-- @return The new object.
function Layer:new( map, params )

	local instance = setmetatable( {}, self )

    instance._map = map

    instance._data = {}
	instance._properties = {}
	instance._chunks = {}

	for k, v in pairs( params ) do
		if k == "properties" then
			for i = 1, #v, 1 do
				local p = Property:new( v[ i ] )
				instance._properties[ p:getName() ] = p
			end
		elseif k ~= "chunks" then
			instance._data[ k ] = v
		end
	end

    for i = 1, #params.chunks or {}, 1 do
        instance._chunks[ i ] = Chunk:new( instance._map, instance, params.chunks[ i ] )
		instance._chunks[ i ]:setOpacity( instance:getOpacity() )
    end

	return instance

end

--- Shows this Layer object.
-- @param position The position of the camera focus.
-- @param bounds The bounds for the camera. Optional, defaults to a table with 'north', 'east', 'south', 'west' all set to 0 so it only shows a chunk that's at the specified position.
function Layer:show( position, bounds )

    if self:isVisible() then

		local chunkPosition, chunkSize

	    bounds = bounds or
	    {
	        east = 0,
	        west = 0,
	        north = 0,
	        south = 0
	    }

		self:hide()

		self._shownChunks = self:getChunksAt( position, bounds )

	    for i = 1, #self._shownChunks, 1 do
	        self._shownChunks[ i ]:show()
	    end

	end

end


--- Hides this Layer object.
function Layer:hide()
	for i = #( self._shownChunks or {} ), 1, -1 do
		local c = remove( self._shownChunks, i )
		c:hide()
	end
end

--- Gets the chunks of this layer.
-- @return The chunks.
function Layer:getChunks()
	return self._chunks
end

--- Shows this Layer object.
-- @param position The position to retrieve chunks from.
-- @param bounds The bounds for the search. Optional, defaults to a table with 'north', 'east', 'south', 'west' all set to 0 so it only returns a chunk that's at the specified position.
-- @return A table of found chunks.
function Layer:getChunksAt( position, bounds )

	local chunks = {}

	local chunkPosition, chunkSize

    bounds = bounds or
    {
        east = 0,
        west = 0,
        north = 0,
        south = 0
    }

    for i = 1, #self._chunks, 1 do

        chunkPosition = self._chunks[ i ]:getContentPosition( true )
        chunkSize = self._chunks[ i ]:getContentSize( true )

        if position.x >= chunkPosition.x - chunkSize.width * ( bounds.east or 0 ) and position.x <= chunkPosition.x + ( chunkSize.width * ( ( bounds.west or 0 ) + 1 ) ) and
            position.y >= chunkPosition.y - chunkSize.height * ( bounds.south or 0 ) and position.y <= chunkPosition.y + ( chunkSize.height * ( ( bounds.north or 0 ) + 1 ) ) then
            chunks[ #chunks + 1 ] = self._chunks[ i ]
        end

    end

	return chunks

end

--- Gets the ID of the layer.
-- @return The ID.
function Layer:getID()
	return self._data.id
end

--- Gets the name of the layer.
-- @return The name.
function Layer:getName()
	return self._data.name
end

--- Gets the opacity of the layer.
-- @return The opacity.
function Layer:getOpacity()
	return self._data.opacity
end

--- Gets the size of the layer.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'width' and 'height' properties or as two separate return values.
function Layer:getSize( asTable )
    if asTable then
		return { width = self:getWidth(), height = self:getHeight() }
	else
		return self:getWidth(), self:getHeight()
	end
end

--- Gets the width of the layer.
-- @return The width.
function Layer:getWidth()
	return self._data.width
end

--- Gets the height of the layer.
-- @return The height.
function Layer:getHeight()
	return self._data.height
end

--- Gets the offset of the layer.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'x' and 'y' properties or as two separate return values.
function Layer:getStartPosition( asTable )
    if asTable then
		return { x = self:getOffsetX(), y = self:getOffsetY() }
	else
		return self:getOffsetX(), self:getOffsetY()
	end
end

--- Gets the x offset of the layer.
-- @return The offset.
function Layer:getOffsetX()
	return self._data.offsetx or 0
end

--- Gets the y offset of the layer.
-- @return The offset.
function Layer:getOffsetY()
	return self._data.offsety or 0
end

--- Gets the start position of the layer.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'x' and 'y' properties or as two separate return values.
function Layer:getStartPosition( asTable )
    if asTable then
		return { x = self:getStartX(), y = self:getStartY() }
	else
		return self:getStartX(), self:getStartY()
	end
end

--- Gets the start x position of the layer.
-- @return The position.
function Layer:getStartX()
	return self._data.startx
end

--- Gets the start y position of the layer.
-- @return The position.
function Layer:getStartY()
	return self._data.starty
end

--- Gets the position of the layer.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'x' and 'y' properties or as two separate return values.
function Layer:getPosition( asTable )
    if asTable then
		return { x = self:getX(), y = self:getY() }
	else
		return self:getX(), self:getY()
	end
end

--- Gets the x position of the layer.
-- @return The position.
function Layer:getX()
	return self._data.x
end

--- Gets the y position of the layer.
-- @return The position.
function Layer:getY()
	return self._data.y
end

--- Checks if this layer visible or not.
-- @return True if it is, false otherwise.
function Layer:isVisible()
    return self._data.visible
end

--- Gets the type of this layer.
-- @return The type.
function Layer:getType()
	return self._data.type
end

--- Gets as custom property of this layer.
-- @param name The name of the property.
-- @return The property.
function Layer:getProperty( name )
	return self._properties[ name ]
end

--- Destroys this Layer object.
function Layer:destroy()

	for i = #self._chunks, 1, -1 do
        local c = remove( self._chunks, i )
        c:destroy()
        c = nil
    end

end

--- Return the Layer class definition.
return Layer
