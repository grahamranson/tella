--[[
The MIT License (MIT)
Copyright (c) 2019 Graham Ranson - www.grahamranson.co.uk / @GrahamRanson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]

--- Class creation.
local Tileset = {}
Tileset.__index = Tileset

--- Required libraries.
local Property = require( "tella.property" )
local Utils = require( "tella.utils" ):new()

--- Localised functions.

--- Initiates a new Tileset object.
-- @param params Paramater table for the object.
-- @return The new object.
function Tileset:new( map, params )

	local instance = setmetatable( {}, self )

    instance._map = map
    instance._firstGID = params.firstgid
    instance._source = params.source
	instance._baseDir = params.baseDir or system.ResourceDirectory
    instance._data = Utils:decodeFile( instance._source, instance._baseDir )

	instance._properties = {}
	for i = 1, #( instance._data.properties or {} ), 1 do
		local p = Property:new( instance._data.properties[ i ] )
		instance._properties[ p:getName() ] = p
	end

    local options =
    {
        width = instance:getTileWidth(),
        height = instance:getTileHeight(),
        numFrames = instance:getTileCount(),
        border = instance:getSpacing(),
        sheetContentWidth = instance:getImageWidth(),
        sheetContentHeight = instance:getImageHeight(),
    }

    instance._imageSheet = graphics.newImageSheet( instance:getImagePath(), instance._baseDir, options )

	return instance

end

--- Gets the image sheet of this tileset.
-- @return The image sheet.
function Tileset:getImageSheet()
    return self._imageSheet
end

--- Creates a new image of a tile for this tileset.
-- @param gid The GID of the tile to create.
-- @return The newly created display object.
function Tileset:getImage( gid )
    return display.newImageRect( self:getImageSheet(), gid, self:getTileWidth(), self:getTileHeight() )
end

--- Gets the first GID of this tileset.
-- @return The GID.
function Tileset:getFirstGID()
    return self._firstGID
end

--- Gets the number of tiles in this tileset.
-- @return The count.
function Tileset:getTileCount()
    return self._data.tilecount
end

--- Gets the size of the tiles used in this tileset.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'width' and 'height' properties or as two separate return values.
function Tileset:getTileSize( asTable )
    if asTable then
		return { width = self:getTileWidth(), height = self:getTileHeight() }
	else
		return self:getTileWidth(), self:getTileHeight()
	end
end

--- Gets the width of the tiles in this tileset.
-- @return The width.
function Tileset:getTileWidth()
	return self._data.tilewidth
end

--- Gets the height of the tiles in this tileset.
-- @return The height.
function Tileset:getTileHeight()
	return self._data.tileheight
end

--- Gets the source of this tileset.
-- @return The source.
function Tileset:getImagePath()
    return self._source
end

--- Gets the path to the image used in this tileset.
-- @return The path.
function Tileset:getImagePath()
    return self._data.image
end

--- Gets the size of the image used for this tileset.
-- @param asTable Set to true if you want the size returned as a table. Optional, defaults to false.
-- @return The size, either as a table with 'width' and 'height' properties or as two separate return values.
function Tileset:getImageSize( asTable )
    if asTable then
		return { width = self:getImageWidth(), height = self:getImageHeight() }
	else
		return self:getImageWidth(), self:getImageHeight()
	end
end

--- Gets the width of the image used for this tileset.
-- @return The width.
function Tileset:getImageWidth()
	return self._data.imagewidth
end

--- Gets the height of the image used for this tileset.
-- @return The height.
function Tileset:getImageHeight()
	return self._data.imageheight
end

--- Gets the margin of this tileset.
-- @return The margin.
function Tileset:getMargin()
	return self._data.margin or 0
end

--- Gets the spacing of this tileset.
-- @return The spacing.
function Tileset:getSpacing()
	return self._data.spacing or 0
end

--- Gets the name of this tileset.
-- @return The name.
function Tileset:getName()
	return self._data.name
end

--- Gets the type of this tileset.
-- @return The type.
function Tileset:getType()
	return self._data.type
end

--- Gets the version number of this tileset.
-- @return The version number.
function Tileset:getVersion()
	return self._data.version
end

--- Gets the version of Tiled this tileset was created by.
-- @return The version number.
function Tileset:getTiledVersion()
	return self._data.tiledversion
end

--- Gets the number of columns.
-- @return The column count.
function Tileset:getColumns()
	return self._data.columns
end

--- Gets as custom property of this tileset.
-- @param name The name of the property.
-- @return The property.
function Tileset:getProperty( name )
	return self._properties[ name ]
end

--- Destroys this Tileset object.
function Tileset:destroy()
	self._imageSheet = nil
end

--- Return the Tileset class definition.
return Tileset
