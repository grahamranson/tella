--[[
The MIT License (MIT)
Copyright (c) 2019 Graham Ranson - www.grahamranson.co.uk / @GrahamRanson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]

--- Class creation.
local Tile = {}
Tile.__index = Tile

--- Required libraries.

--- Localised functions.
local floor = math.floor

--- Initiates a new Tile object.
-- @param params Paramater table for the object.
-- @return The new object.
function Tile:new( map, params )

	local instance = setmetatable( {}, self )

    instance._map = map

    for k, v in pairs( params ) do
        instance[ "_" .. k ] = v
    end

	return instance

end

--- Shows this Tile object.
function Tile:show()

    if not self:getVisual() then
        if self._id ~= 0 then
            self._visual = self._map:getTilesetFromGID( self._id ):getImage( self._id )
            self._visual.x = floor( self._x * self._width )
            self._visual.y = floor( self._y * self._height )
        end
    end

    if self:getVisual() then
        self:getVisual().isVisible = true
    end

end


--- Hides this Tile object.
function Tile:hide()
    if self:getVisual() then
        self:getVisual().isVisible = false
    end
end

--- Gets the display object of this Tile.
-- @return The display object.
function Tile:getVisual()
    return self._visual
end

--- Destroys this Tile object.
function Tile:destroy()
    display.remove( self._visual )
    self._visual = nil
end

--- Return the Tile class definition.
return Tile
