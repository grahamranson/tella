--[[
The MIT License (MIT)
Copyright (c) 2019 Graham Ranson - www.grahamranson.co.uk / @GrahamRanson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]

--- Class creation.
local Property = {}
Property.__index = Property

--- Required libraries.

--- Localised functions.

--- Initiates a new Property object.
-- @return The new object.
function Property:new( params )

	local instance = setmetatable( {}, self )

	instance._data = params

	return instance

end

--- Gets the name of this property.
-- @return The name.
function Property:getName()
	return self._data.name
end

--- Gets the value of this property.
-- @return The value.
function Property:getValue()
	return self._data.value
end

--- Gets the type of this property.
-- @return The type.
function Property:getType()
	return self._data.type
end

--- Destroys this Property object.
function Property:destroy()

end

--- Return the Property class definition.
return Property
